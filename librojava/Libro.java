public class Libro{
    private Integer isbn;
    private String titulo;
    private String autor;
    private Integer numeroPaginas;

//Métodos setters (cargar valores a los atributos de clase) y getters (visualizar valores de atributos de clase)

//Setters   
    public void setIsbn(int codigo){
        this.isbn=codigo;
    }
 
    public void setTitulo(String tituloLibro){
        this.titulo=tituloLibro;
    }
     
    public void setAutor(String autorLibro){
        this.autor=autorLibro;
    }

    public void setNumeroPaginas(int numPags){
        this.numeroPaginas=numPags;
    }


 //Getters
    
    public Integer getIsbn(){
    return isbn;
    }
    
    public String getTitulo(){
        return titulo;
    }

    public String getAutor(){
        return autor;
    }

    public Integer getNumeroPaginas(){
        return numeroPaginas;
    }

    //toString puede retornar un mensaje sobre el estado del objeto, tanto int como string de los valores de atributos
    public String toString() {
        return "El libro con Isbn: "+isbn+" creado por el autor: "+autor+" tiene: "+numeroPaginas+" paginas";
    }

} 

