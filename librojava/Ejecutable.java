
public class Ejecutable {

    public static void main (String[] args){
        Libro libro1=new Libro();
        Libro libro2=new Libro();
//Los numeros cargados de ISBN los cargue por mi cuenta, ya que los oficiales son largos para el tipo de dato Integer    
       libro1.setIsbn(9517538);
       libro1.setTitulo("Cien años de soledad"); 
       libro1.setAutor("Gabriel Garcia Marquez");
       libro1.setNumeroPaginas(471);

       libro2.setIsbn(4563217);
       libro2.setTitulo("El Quijote de la Mancha");
       libro2.setAutor("Miguel De Cervantes Saavedra");
       libro2.setNumeroPaginas(592);

       System.out.println("\nLa informacion del libro 1 es: "+ libro1.toString());
       System.out.println("\nLa informacion del libro 2 es: "+ libro2.toString());

    }

    
}
