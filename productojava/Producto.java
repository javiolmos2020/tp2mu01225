public class Producto{
    private int codigoProducto;
    private String nombreProducto;
    private double  precioCosto;
    private double pctGcia;
    private double  precioVta;

    public void setCod(int codProd){
    this.codigoProducto=codProd;
    }
    public int getCod(){
        return codigoProducto;
    }

    public void setNomProd(String nProd){
    this.nombreProducto=nProd;       
    }
    public String getNombreProd(){
        return nombreProducto;
    }

    public void setPcioCost(double pCosto ){
        this.precioCosto=pCosto;       
        }
    public double getPcioCost(){
        return precioCosto;
    }

    public void setPctGcia(double vpctGcia){
        this.pctGcia=vpctGcia;
    }

    public double getPctGcia(){
        return pctGcia;
    }
    
    public void setPcioVta(double pVenta){
        this.precioVta=pVenta; 
    }

    public double getPcioVta(){
        return precioVta; 
    }


    public String toString(){
        return  "El codigo es: "+ codigoProducto + ".Nombre: "+ nombreProducto +" .Su precio es: "+ precioCosto +".Su precio de venta es: "+ precioVta;
    }
}